<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__usb__dev__functions__data__transfer" kind="group">
    <compoundname>group_usb_dev_functions_data_transfer</compoundname>
    <title>Data Transfer Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__data__transfer_1ga87f178d2a65510bd26dff4ee5d2c06ab" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref></type>
        <definition>cy_en_usb_dev_status_t Cy_USB_Dev_StartReadEp</definition>
        <argsstring>(uint32_t endpoint, cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_StartReadEp</name>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Start a reading on a certain endpoint. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The OUT data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The read is not allowed for OUT endpoints after SET_CONFIGURATION or SET_INTERFACE request therefore this function must be called before reading data from OUT endpoints. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="516" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="494" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1312" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__data__transfer_1ga0ffcc053f1154d6f77b1744f7a517ef5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref></type>
        <definition>cy_en_usb_dev_status_t Cy_USB_Dev_ReadEpBlocking</definition>
        <argsstring>(uint32_t endpoint, uint8_t *buffer, uint32_t size, uint32_t *actSize, int32_t timeout, cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_ReadEpBlocking</name>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>actSize</declname>
        </param>
        <param>
          <type>int32_t</type>
          <declname>timeout</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Read data received from USB Host from a certain endpoint. </para>        </briefdescription>
        <detaileddescription>
<para>Before calling this function, <ref kindref="member" refid="group__group__usb__dev__functions__data__transfer_1ga87f178d2a65510bd26dff4ee5d2c06ab">Cy_USB_Dev_StartReadEp</ref> must be called. This function is blocking and returns after successful USB Host transfer, or an error or timeout occurred.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The OUT data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to buffer that stores data that was read. <linebreak />
Allocate buffer using <ref kindref="member" refid="group__group__usb__dev__macros_1ga0ccd70a937ab3d69aeb40030713bc69b">CY_USB_DEV_ALLOC_ENDPOINT_BUFFER</ref> macro to make it USBFS driver configuration independent (See <ref kindref="member" refid="index_1group_usb_dev_ep_buf_alloc">Allocate Data Endpoint Buffer</ref> for more information).</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of bytes to read. This value must be less or equal to endpoint maximum packet size.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>actSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of bytes that were actually read.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeout</parametername>
</parameternamelist>
<parameterdescription>
<para>Defines in milliseconds the time for which this function can block. If that time expires the function returns. To wait forever pass <ref kindref="member" refid="group__group__usb__dev__macros_1gad8a978b86bb53dd8e46d9f9db9399ee8">CY_USB_DEV_WAIT_FOREVER</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="623" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="559" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1315" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__data__transfer_1ga5f3bb59175bd8b46a558b8206d6f9cdf" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref></type>
        <definition>cy_en_usb_dev_status_t Cy_USB_Dev_ReadEpNonBlocking</definition>
        <argsstring>(uint32_t endpoint, uint8_t *buffer, uint32_t size, uint32_t *actSize, cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_ReadEpNonBlocking</name>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>actSize</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Read data received from USB Host from a certain endpoint. </para>        </briefdescription>
        <detaileddescription>
<para>Before calling this function, <ref kindref="member" refid="group__group__usb__dev__functions__data__transfer_1ga87f178d2a65510bd26dff4ee5d2c06ab">Cy_USB_Dev_StartReadEp</ref> must be called.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The OUT data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to buffer that stores data that was read. <linebreak />
Allocate buffer using <ref kindref="member" refid="group__group__usb__dev__macros_1ga0ccd70a937ab3d69aeb40030713bc69b">CY_USB_DEV_ALLOC_ENDPOINT_BUFFER</ref> macro to make it USBFS driver configuration independent (See <ref kindref="member" refid="index_1group_usb_dev_ep_buf_alloc">Allocate Data Endpoint Buffer</ref> for more information).</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of bytes to read. This value must be less than or equal to endpoint maximum packet size.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>actSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of bytes that were actually read.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="689" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="659" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1322" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__data__transfer_1gafe804a021cfbea888a966b8cf1b4172e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref></type>
        <definition>cy_en_usb_dev_status_t Cy_USB_Dev_AbortEpTransfer</definition>
        <argsstring>(uint32_t endpoint, cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_AbortEpTransfer</name>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Aborts pending read or write endpoint operation. </para>        </briefdescription>
        <detaileddescription>
<para>If there is any bus activity after abort operation requested the function waits for its completion or timeout. The timeout is time to transfer bulk or interrupt packet of maximum playload size. If this bus activity is a transfer to the aborting endpoint the received data is lost and endpoint transfer completion callbacks is not invoked. After function returns new read or write endpoint operation can be submitted.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The abort operation is not supported for ISOC endpoints because these endpoints do not have handshake and are always accessible by the USB Host. Therefore, abort can cause unexpected behavior. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="467" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="453" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1328" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__data__transfer_1ga79a6fbd0663556eb7ccc586e6dbe7e4e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_USB_Dev_GetEpNumToRead</definition>
        <argsstring>(uint32_t endpoint, cy_stc_usb_dev_context_t const *context)</argsstring>
        <name>Cy_USB_Dev_GetEpNumToRead</name>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the number of bytes that available to be read from a certain endpoint buffer. </para>        </briefdescription>
        <detaileddescription>
<para>Before calling this function ensure that the Host wrote data into the endpoint. The returned value is updated after the Host access to the endpoint but remains unchanged after data has been read from the endpoint buffer.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The OUT data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Number of bytes in OUT endpoint buffer. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1864" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" bodystart="1860" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1331" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__data__transfer_1ga1bf916540d58d458568b2eed1a186df4" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref></type>
        <definition>cy_en_usb_dev_status_t Cy_USB_Dev_WriteEpBlocking</definition>
        <argsstring>(uint32_t endpoint, uint8_t const *buffer, uint32_t size, int32_t timeout, cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_WriteEpBlocking</name>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type>uint8_t const *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type>int32_t</type>
          <declname>timeout</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Write data to be transferred to USB Host from a certain endpoint. </para>        </briefdescription>
        <detaileddescription>
<para>This function is blocking and returns after successful USB Host transfer, or an error or timeout occurred.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The IN data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the buffer containing data bytes to write. <linebreak />
Allocate buffer using <ref kindref="member" refid="group__group__usb__dev__macros_1ga0ccd70a937ab3d69aeb40030713bc69b">CY_USB_DEV_ALLOC_ENDPOINT_BUFFER</ref> macro to make it USBFS driver configuration independent (See <ref kindref="member" refid="index_1group_usb_dev_ep_buf_alloc">Allocate Data Endpoint Buffer</ref> for more information).</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of bytes to write. This value must be less than or equal to endpoint maximum packet size.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeout</parametername>
</parameternamelist>
<parameterdescription>
<para>Defines in milliseconds the time for which this function can block. If that time expires, the function returns. To wait forever, pass <ref kindref="member" refid="group__group__usb__dev__macros_1gad8a978b86bb53dd8e46d9f9db9399ee8">CY_USB_DEV_WAIT_FOREVER</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="792" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="728" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1335" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__data__transfer_1gafca5c6c661b90572a5278322b0773ba6" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref></type>
        <definition>cy_en_usb_dev_status_t Cy_USB_Dev_WriteEpNonBlocking</definition>
        <argsstring>(uint32_t endpoint, uint8_t const *buffer, uint32_t size, cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_WriteEpNonBlocking</name>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type>uint8_t const *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Write data to be transferred to USB Host from a certain endpoint. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The IN data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the buffer containing data bytes to write. <linebreak />
Allocate buffer using <ref kindref="member" refid="group__group__usb__dev__macros_1ga0ccd70a937ab3d69aeb40030713bc69b">CY_USB_DEV_ALLOC_ENDPOINT_BUFFER</ref> macro to make it USBFS driver configuration independent (See <ref kindref="member" refid="index_1group_usb_dev_ep_buf_alloc">Allocate Data Endpoint Buffer</ref> for more information).</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of bytes to write. This value must be less than or equal to endpoint maximum packet size.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="852" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="824" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1341" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__data__transfer_1ga17873db332970f17f9cfa8b7f383c019" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_USB_Dev_OverwriteHandleTimeout</definition>
        <argsstring>(cy_fn_usb_dev_handle_timeout_ptr_t handleTimeout, cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_OverwriteHandleTimeout</name>
        <param>
          <type><ref kindref="member" refid="group__group__usb__dev__structures__func__ptr_1gab48bb4326ac3019669c6697714b1c377">cy_fn_usb_dev_handle_timeout_ptr_t</ref></type>
          <declname>handleTimeout</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Overwrite the handle timeout function that is implemented internally. </para>        </briefdescription>
        <detaileddescription>
<para>The internal implementation converts one timeout unit to milliseconds.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>handleTimeout</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to function to be executed to handle timeout for blocking operations.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1889" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" bodystart="1885" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1346" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>
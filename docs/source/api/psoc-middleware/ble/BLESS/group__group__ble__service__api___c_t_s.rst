=============================
Current Time Service (CTS)
=============================

.. doxygengroup:: group_ble_service_api_CTS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___c_t_s__server__client.rst
   group__group__ble__service__api___c_t_s__server.rst
   group__group__ble__service__api___c_t_s__client.rst
   group__group__ble__service__api___c_t_s__definitions.rst
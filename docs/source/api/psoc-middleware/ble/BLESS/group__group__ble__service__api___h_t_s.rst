=================================
Health Thermometer Service (HTS)
=================================

.. doxygengroup:: group_ble_service_api_HTS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___h_t_s__server__client.rst
   group__group__ble__service__api___h_t_s__server.rst
   group__group__ble__service__api___h_t_s__client.rst
   group__group__ble__service__api___h_t_s__definitions.rst
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__common___intr__feature__api__functions" kind="group">
    <compoundname>group_ble_common_Intr_feature_api_functions</compoundname>
    <title>BLE Interrupt Notification Callback</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__common___intr__feature__api__functions_1ga4f1bb36b7bd9cd99410c677a62da061e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_RegisterInterruptCallback</definition>
        <argsstring>(uint32_t intrMask, cy_ble_intr_callback_t CallBack)</argsstring>
        <name>Cy_BLE_RegisterInterruptCallback</name>
        <param>
          <type>uint32_t</type>
          <declname>intrMask</declname>
        </param>
        <param>
          <type>cy_ble_intr_callback_t</type>
          <declname>CallBack</declname>
        </param>
        <briefdescription>
<para>This function registers a callback to expose BLE interrupt notifications to an application that indicates a different link layer and radio state transition to the user from the BLESS interrupt context. </para>        </briefdescription>
        <detaileddescription>
<para>This callback is triggered at the beginning of a received BLESS interrupt (based on the registered interrupt mask).</para><para>An application can use an interrupt callback to know when:<itemizedlist>
<listitem><para>the RF activity is about to begin/end;</para></listitem><listitem><para>the BLE device changes its state from advertising to connected;</para></listitem><listitem><para>BLESS transits between BLESS active and BLESS low-power modes.</para></listitem></itemizedlist>
</para><para>These BLESS real-time states can be used to synchronize an application with the BLESS or prevent radio interference with other peripherals, etc.</para><para>BLE dual CPU mode requires an additional configuration IPC channel and IPC Interrupt structure to send notifications from the controller core to Host core. Refer to <ref kindref="member" refid="group__group__ble__common___intr__feature__api__functions_1ga2431c8315bbb2d7dee6709c6b0af65be">Cy_BLE_ConfigureIpcForInterruptCallback()</ref> for details.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The user must call &lt;a href="group__group__ble__common___intr__feature__api__functions.html#group__group__ble__common___intr__feature__api__functions_1ga39785f5666e0ffad99d2d8327cdd669b"&gt;Cy_BLE_IntrNotifyIsrHandler()&lt;/a&gt; inside the user-defined BLESS interrupt service routine (ISR)&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>intrMask</parametername>
</parameternamelist>
<parameterdescription>
<para>All interrupts masks are specified in the <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga716607fd63ab34c49af7a84e76239640">cy_en_ble_interrupt_callback_feature_t</ref> enumeration.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>CallBack</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to an application notify callback.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref> : The return value indicates whether the function succeeded or failed. The possible error codes:</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The callback is registered successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameters failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="550" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble.c" bodystart="532" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble.h" line="472" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___intr__feature__api__functions_1ga9d1d62eec83ccdad2828712553359db8" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_UnRegisterInterruptCallback</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_BLE_UnRegisterInterruptCallback</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function un-registers the callback that exposed BLE interrupt notifications to the application. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref> : Return value indicates whether the function succeeded or failed. The following are possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The callback registered successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The IPC channel is busy (BLE dual CPU mode only). &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="665" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble.c" bodystart="651" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble.h" line="474" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___intr__feature__api__functions_1ga2431c8315bbb2d7dee6709c6b0af65be" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_ConfigureIpcForInterruptCallback</definition>
        <argsstring>(uint32_t ipcChan, uint32_t ipcIntr, uint32_t ipcIntrPrior)</argsstring>
        <name>Cy_BLE_ConfigureIpcForInterruptCallback</name>
        <param>
          <type>uint32_t</type>
          <declname>ipcChan</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>ipcIntr</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>ipcIntrPrior</declname>
        </param>
        <briefdescription>
<para>This function configures IPC channel for BLE interrupt notifications feature, when BLE is operating in BLE dual CPU mode. </para>        </briefdescription>
        <detaileddescription>
<para>This function must be called only on the Host CPU core.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>ipcChan</parametername>
</parameternamelist>
<parameterdescription>
<para>IPC channel. Valid range: 9..15 </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>ipcIntr</parametername>
</parameternamelist>
<parameterdescription>
<para>IPC Interrupt structure. Valid range: 9..15 </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>ipcIntrPrior</parametername>
</parameternamelist>
<parameterdescription>
<para>IPC Interrupt priority. Valid range: 0..7</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref> : The return value indicates whether the function succeeded or failed. The possible error codes:</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The callback is registered successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameters failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The IPC channel is busy (BLE dual CPU mode only). &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="631" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble.c" bodystart="577" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble.h" line="476" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___intr__feature__api__functions_1ga39785f5666e0ffad99d2d8327cdd669b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_BLE_IntrNotifyIsrHandler</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_BLE_IntrNotifyIsrHandler</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function sends notification (with interrupt type information) to user callback for the BLE interrupts subscribed by user. </para>        </briefdescription>
        <detaileddescription>
<para>The list of supported interrupts described in the enumeration cy_en_ble_interrupt_callback_feature_t. <ref kindref="member" refid="group__group__ble__common___intr__feature__api__functions_1ga4f1bb36b7bd9cd99410c677a62da061e">Cy_BLE_RegisterInterruptCallback()</ref> API used to register callback for receiving the BLE interrupts.</para><para>This function must be called inside the user-defined BLESS interrupt service routine (ISR) if user uses Interrupt Notifications Feature. </para>        </detaileddescription>
        <inbodydescription>
<para>Updates the firmware and hardware to exit sleep mode, when called from the interrupt mode, after checking the state machine.</para>        </inbodydescription>
        <location bodyend="164" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_hal_int.c" bodystart="36" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble.h" line="480" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>API exposes BLE interrupt notifications to the application which indicates a different link layer and radio state transitions to the user from the BLESS interrupt context. </para>    </briefdescription>
    <detaileddescription>
<para>The user registers for a particular type of a callback and the PSoC 6 BLE Middleware will call that registered callback basing on the registered mask. Refer to section <ref kindref="member" refid="page_ble_section_more_information_1group_ble_interrupt_notify">BLE Interrupt Notification Callback</ref>. </para>    </detaileddescription>
  </compounddef>
</doxygen>
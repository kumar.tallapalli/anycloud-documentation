<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__common___h_c_i__api__functions" kind="group">
    <compoundname>group_ble_common_HCI_api_functions</compoundname>
    <title>BLE HCI API</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__common___h_c_i__api__functions_1ga2963d8a11c1aac9442e171e6e33e34fb" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_SoftHciSendAppPkt</definition>
        <argsstring>(cy_stc_ble_hci_tx_packet_info_t *HciPktParams)</argsstring>
        <name>Cy_BLE_SoftHciSendAppPkt</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__hci__tx__packet__info__t">cy_stc_ble_hci_tx_packet_info_t</ref> *</type>
          <declname>HciPktParams</declname>
        </param>
        <briefdescription>
<para>This function sends a HCI packet to the BLE Stack's Controller when the Soft Transport feature is enabled using the Cy_BLE_SoftHciTransportEnable() API. </para>        </briefdescription>
        <detaileddescription>
<para>Application should allocate memory for the buffer to hold the HCI packet passed as an input parameter. This API copies the HCI packet into the controller's HCI buffer. Hence, the Application may deallocate the memory buffer created to hold the HCI packet, once the API returns.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>HciPktParams</parametername>
</parameternamelist>
<parameterdescription>
<para>HCI packet which the application wants to push to the BLE Stack's Controller.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t : Returns whether the controller is active or not.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Return value &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CYBLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;HciPktParams Parameter is NULL. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation not permitted. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CYBLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack.h" line="2726" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___h_c_i__api__functions_1ga1651ae819f69544defe83d426b90d900" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_RegisterHciCb</definition>
        <argsstring>(cy_ble_hci_rx_cb_t param)</argsstring>
        <name>Cy_BLE_RegisterHciCb</name>
        <param>
          <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga153d929b7fc525f53f9d29fba0e34179">cy_ble_hci_rx_cb_t</ref></type>
          <declname>param</declname>
        </param>
        <briefdescription>
<para>This function will initialize the HCI layer of the BLE Host and will register Rx callback for all HCI events and data. </para>        </briefdescription>
        <detaileddescription>
<para>This function must be called before calling <ref kindref="member" refid="group__group__ble__common___h_c_i__api__functions_1ga11cd19bb7216532af621ab4d838cc7d4">Cy_BLE_HciTransmit()</ref>. This function should not be called during normal BLE Stack operation. If an application randomly uses HCI functions along with other functions, behavior will be unpredictable.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>param</parametername>
</parameternamelist>
<parameterdescription>
<para>parameter is of type 'cy_ble_hci_rx_cb_t', callback function for the packets received from the controller. Please refer to BLE Core 5.0 Specification, Vol 2, Part E Section 7 for details on HCI event/data packet format.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t : Return value indicates whether the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On specifying NULL as input parameter. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_host_main.h" line="2303" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___h_c_i__api__functions_1ga11b6e316b6df61ac9bd5740e1809eed3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_BLE_HciShutdown</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_BLE_HciShutdown</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function stops any ongoing operation on the BLE Stack and forces the BLE Host HCI and the BLE controller to shut down. </para>        </briefdescription>
        <detaileddescription>
<para>The only functions that can be called after calling this function is Cy_BLE_StackInit() or <ref kindref="member" refid="group__group__ble__common___h_c_i__api__functions_1ga1651ae819f69544defe83d426b90d900">Cy_BLE_RegisterHciCb()</ref>.</para><para>No event is generated on calling this function.</para><para><simplesect kind="return"><para>None </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_host_main.h" line="2322" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___h_c_i__api__functions_1ga11cd19bb7216532af621ab4d838cc7d4" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_HciTransmit</definition>
        <argsstring>(cy_stc_ble_hci_tx_packet_info_t *param)</argsstring>
        <name>Cy_BLE_HciTransmit</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__hci__tx__packet__info__t">cy_stc_ble_hci_tx_packet_info_t</ref> *</type>
          <declname>param</declname>
        </param>
        <briefdescription>
<para>The application must use this function for sending HCI command/data packet to the controller. </para>        </briefdescription>
        <detaileddescription>
<para>Refer to BLE Core 5.0 Specification, Vol 2, Part E Section 7 for details on HCI command/data packet format.</para><para>This function must be called after calling '<ref kindref="member" refid="group__group__ble__common___h_c_i__api__functions_1ga1651ae819f69544defe83d426b90d900">Cy_BLE_RegisterHciCb()</ref>'.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>param</parametername>
</parameternamelist>
<parameterdescription>
<para>parameter is of type '<ref kindref="compound" refid="structcy__stc__ble__hci__tx__packet__info__t">cy_stc_ble_hci_tx_packet_info_t</ref>'</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t : Return value indicates whether the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On specifying NULL as input parameter. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_host_main.h" line="2349" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>API exposes BLE Stack's Host HCI to user, if they want to do DTM testing or use BLE Controller alone. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>
========================================
wiced_bt_l2cap_cfg_information_t Struct
========================================

.. doxygenstruct:: wiced_bt_l2cap_cfg_information_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
==============
API Reference
==============


The following provides a list of API documentation

+----------------------------------+----------------------------------+
| `Logical Link Control and        | Logical Link Control and         |
| Adaptation Protocol              | Adaptation Layer Protocol,       |
| (L2CAP) <group__l2cap.html>`__   | referred to as L2CAP, provides   |
|                                  | connection oriented and          |
|                                  | connectionless data services to  |
|                                  | upper layer protocols with       |
|                                  | protocol multiplexing capability |
|                                  | and segmentation and reassembly  |
|                                  | operation                        |
+----------------------------------+----------------------------------+
| `Device                          | This section consists of several |
| Management <group__wicedbt___    | management entities:             |
| device_management.html>`__       |                                  |
+----------------------------------+----------------------------------+
| `Advertisement &                 | This section provides functions  |
| Scan <group__btm__bl             | for BLE advertisement and BLE    |
| e__adv__scan__functions.html>`__ | scan operations                  |
+----------------------------------+----------------------------------+
| `Connection and                  | This section provides functions  |
| Whitelist <group__btm__ble__con  | for BLE connection related and   |
| n__whitelist__functions.html>`__ | whitelist operations             |
+----------------------------------+----------------------------------+
| `Phy <group__b                   | This section provides            |
| tm__ble__phy__functions.html>`__ | functionality to read and update |
|                                  | PHY                              |
+----------------------------------+----------------------------------+
| `MultiAdv <group__btm__ble       | This section describes Multiple  |
| __multi__adv__functions.html>`__ | Advertisement API, using this    |
|                                  | interface application can enable |
|                                  | more than one advertisement train|
+----------------------------------+----------------------------------+
| `Utilities <group__wicedbt     \ | This sections provides Bluetooth |
| __utility.html>`__               | utilities functions related to   |
|                                  | trace, local bda, tx power etc   |
+----------------------------------+----------------------------------+
| `BLE                             | BLE Security API                 |
| Security <group__btm__b          |                                  |
| le__sec__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Bluetooth Stack Initialize & \  | This section describes API and   |
| Configuration                 \  | Data structures required to      |
| <group__wiced__bt__cfg.html>`__  | initialize and configure the     |
|                                  | BT-Stack                         |
+----------------------------------+----------------------------------+
| `Generic Attribute               | Generic Attribute (GATT)         |
| (GATT)                           | Functions                        |
| <group__wicedbt__gatt.html>`__   |                                  |
+----------------------------------+----------------------------------+
| `Server API                      | BLE (Bluetooth Low Energy)       |
| <group__gatt__server__           | Specific functions               |
| api__functions.html>`__          |                                  |
+----------------------------------+----------------------------------+
| `GATT Server Data                | GATT Server Data API             |
| API <group_                      |                                  |
| _gattsr__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `GATT                            | GATT Database Access Functions   |
| Database <group_                 |                                  |
| _gattdb__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `GATT Robust                     | GATT Robust Caching API          |
| Caching <group__gatt__robust__   |                                  |
| caching__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Client                          | GATT Profile Client Functions    |
| API <group__gatt_                |                                  |
| _client__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Connection API                  | GATT Profile Connection          |
| <group__gatt__                   | Functions                        |
| common__api.html>`__             |                                  |
+----------------------------------+----------------------------------+
| `EATT <group                     | EATT API                         |
| __gatt__eatt__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Bluetooth Stack Platform        | Interface between Stack and      |
| Interface <group__wic            | platform                         |
| ed__bt__platform__group.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Memory                          | Helper APIs to create heaps and  |
| Manage\                          | pools and allocate/free buffers  |
| ment <group__wiced__mem.html>`__ | from those pools or heaps        |
+----------------------------------+----------------------------------+
| `Common Bluetooth                | Common Bluetooth definitions     |
| definitions                      |                                  |
| <group__gentypes.html>`__        |                                  |
+----------------------------------+----------------------------------+
| `Wiced BT                        | WICED BT Types                   |
| Types <group__bt__types.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `WICED                           | **Result types** See             |
| result <group___result.html>`__  | wiced_result.h                   |
+----------------------------------+----------------------------------+
| `Timer Management                | Defines the interfaces for Timer |
| Services <group__timer.html>`__  | Management Services              |
+----------------------------------+----------------------------------+


.. toctree::
   :hidden:
   
   group__l2cap.rst
   group__wicedbt___device_management.rst
   group__wiced__bt__cfg.rst
   group__wicedbt__gatt.rst
   group__wiced__bt__platform__group.rst
   group__wiced__mem.rst
   group__gentypes.rst
   group__timer.rst
   
   

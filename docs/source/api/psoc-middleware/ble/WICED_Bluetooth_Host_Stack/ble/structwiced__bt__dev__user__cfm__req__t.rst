==========================================
wiced_bt_dev_user_cfm_req_t Struct
==========================================

.. doxygenstruct:: wiced_bt_dev_user_cfm_req_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
=====================
GATT Server Data API
=====================

.. doxygengroup:: gattsr_api_functions
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
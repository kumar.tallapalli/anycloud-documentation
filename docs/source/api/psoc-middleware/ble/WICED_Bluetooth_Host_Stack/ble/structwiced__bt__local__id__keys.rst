==========================================
wiced_bt_local_id_keys struct
==========================================

.. doxygenstruct:: wiced_bt_local_id_keys
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
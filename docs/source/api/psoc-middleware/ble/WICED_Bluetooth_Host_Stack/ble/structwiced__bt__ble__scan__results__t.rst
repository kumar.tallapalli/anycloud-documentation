=================================
wiced_bt_ble_advert_elem_t Struct
=================================

.. doxygenstruct:: wiced_bt_ble_advert_elem_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
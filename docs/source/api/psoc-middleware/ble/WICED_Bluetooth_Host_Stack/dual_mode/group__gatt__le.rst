===========================
BLE (Bluetooth Low Energy)
===========================

.. doxygengroup:: gatt_le
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
==============
API Reference
==============

The following provides a list of API documentation

+----------------------------------+----------------------------------+
| `Logical Link Control and        | Logical Link Control and         |
| Adaptation Protocol              | Adaptation Layer Protocol,       |
| (L2CAP) <group__l2cap.html>`__   | referred to as L2CAP, provides   |
|                                  | connection oriented and          |
|                                  | connectionless data services to  |
|                                  | upper layer protocols with       |
|                                  | protocol multiplexing capability |
|                                  | and segmentation and reassembly  |
|                                  | operation                        |
+----------------------------------+----------------------------------+
| `Common <group__l2cap_           | Commonly used API's for both     |
| _common__api__functions.html>`__ | BE/EDR and LE                    |
|                                  | `L2CAP <group__l2cap.html>`__    |
+----------------------------------+----------------------------------+
| `BR/EDR <group__l2cap__          | API's used for BR/EDR            |
| br__edr__api__functions.html>`__ | `L2CAP <group__l2cap.html>`__    |
+----------------------------------+----------------------------------+
| `BLE <group__l2                  | API's used for LE                |
| cap__le__api__functions.html>`__ | `L2CAP <group__l2cap.html>`__    |
+----------------------------------+----------------------------------+
| `Audio/Video Distribution        | This section describes the API's |
| Transport                        | of Audio/Video Distribution      |
| (AVDT                            | Transport protocol               |
| ) <group__wicedbt__avdt.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Helper                          | This section describes the API's |
| Functions <group__w              | to find A2DP service and APIs to |
| icedbt__av__a2d__helper.html>`__ | parse/encode Codec Specific      |
|                                  | Information Elements for SBC     |
|                                  | Codec, MPEG-1,2 Audio Codec or   |
|                                  | MPEG-2,4 AAC Codec               |
+----------------------------------+----------------------------------+
| `MPEG-1,2                        | This section describes A2DP      |
| Support <group__wi               | MPEG-1,2 Audio codec API         |
| cedbt__a2dp__mpeg__1__2.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `MPEG-2,4 AAC                    | This section describes A2DP      |
| Support <group__wi               | MPEG-2,4 AAC Audio codec API     |
| cedbt__a2dp__mpeg__2__4.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `A2DP SBC                        | This section describes A2DP Low  |
| Support <gr                      | complexity subband codec (SBC)   |
| oup__wicedbt__a2dp__sbc.html>`__ | API                              |
+----------------------------------+----------------------------------+
| `Audio/Video Remote Control      | This section describes the API's |
| (AVRC                            | to use Audio/Video Remote        |
| ) <group__wicedbt__avrc.html>`__ | Control Profile commands which   |
|                                  | use underlying AVCT protocol     |
+----------------------------------+----------------------------------+
| `Bluetooth Stack Initialize &    | This section describes API and   |
| Configuration <g                 | Data structures required to      |
| roup__wiced__bt__cfg.html>`__    | initialize and configure the     |
|                                  | BT-Stack                         |
+----------------------------------+----------------------------------+
| `Device                          | This section consists of several |
| Management <group__wice          | management entities:             |
| dbt___device_management.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `BLE (Bluetooth Low              | This section describes the API's |
| Energy) <group__b                | to use BLE functionality such as |
| tm__ble__api__functions.html>`__ | advertisement, scanning BLE      |
|                                  | Connection, Data transfer, BLE   |
|                                  | Security etc                     |
+----------------------------------+----------------------------------+
| `Advertisement &                 | This section provides functions  |
| Scan <group__btm__bl             | for BLE advertisement and BLE    |
| e__adv__scan__functions.html>`__ | scan operations                  |
+----------------------------------+----------------------------------+
| `Connection and                  | This section provides functions  |
| Whitelist <group__btm__ble__con  | for BLE connection related and   |
| n__whitelist__functions.html>`__ | whitelist operations             |
+----------------------------------+----------------------------------+
| `Phy <group__b                   | This section provides            |
| tm__ble__phy__functions.html>`__ | functionality to read and update |
|                                  | PHY                              |
+----------------------------------+----------------------------------+
| `MultiAdv <group__btm__ble       | This section describes Multiple  |
| __multi__adv__functions.html>`__ | Advertisement API, using this    |
|                                  | interface application can enable |
|                                  | more than one advertisement      |
|                                  | train                            |
+----------------------------------+----------------------------------+
| `BLE                             | Bluetooth LE security API        |
| Security <group__btm__b          | (authorisation, authentication   |
| le__sec__api__functions.html>`__ | and encryption)                  |
+----------------------------------+----------------------------------+
| `Generic Security                | Bluetooth generic security API   |
| API <group__ble__comm            |                                  |
| on__sec__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `BR/EDR (Bluetooth Basic         | BR/EDR (Bluetooth Basic Rate /   |
| Rate / Enhanced Data             | Enhanced Data Rate) Functions    |
| Rate)                            |                                  |
| <group__wicedbt__bredr.html>`__  |                                  |
+----------------------------------+----------------------------------+
| `Bluetooth BR/EDR                | This module provided various     |
| API <g                           | Bluetooth security functionality |
| roup__wiced__bredr__api.html>`__ | such as authorisation,           |
|                                  | authentication and encryption    |
+----------------------------------+----------------------------------+
| `BR/EDR Security                 | This module provided various     |
| Function <group__br__e           | Bluetooth BR/EDR security        |
| dr__sec__api__functions.html>`__ | functionality such as            |
|                                  | authorisation, authentication    |
|                                  | and encryption                   |
+----------------------------------+----------------------------------+
| `Generic Security                | Bluetooth generic security API   |
| API <group__ble__comm            |                                  |
| on__sec__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Utilities <g                    | This sections provides Bluetooth |
| roup__wicedbt__utility.html>`__  | utilities functions related to   |
|                                  | trace, local bda, tx power etc   |
+----------------------------------+----------------------------------+
| `Generic Attribute               | Generic Attribute (GATT)         |
| (GATT                            | Functions                        |
| ) <group__wicedbt__gatt.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Server                          | GATT Profile Server Functions    |
| API <group__gatt_                |                                  |
| _server__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `GATT Server Data                | GATT Server Data API             |
| API <group_                      |                                  |
| _gattsr__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `GATT                            | GATT Database Access Functions   |
| Database <group_                 |                                  |
| _gattdb__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `GATT Robust                     | GATT Robust Caching API          |
| Caching <group__gatt__robust__   |                                  |
| caching__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Client                          | GATT Profile Client Functions    |
| API <group__gatt_                |                                  |
| _client__api__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Connection                      | GATT Profile Connection          |
| API <g                           | Functions                        |
| roup__gatt__common__api.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `BLE (Bluetooth Low              | BLE (Bluetooth Low Energy)       |
| E                                | Specific functions               |
| nergy) <group__gatt__le.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `BR/EDR (Bluetooth Basic Rate\   | BR/EDR (Bluetooth Basic Rate     |
| Enhanced Data                 \  | Enhanced Data Rate) Specific     |
| Rate) <group__gatt__br.html>`__  | functions                        |
+----------------------------------+----------------------------------+
| `EATT <group                     | EATT API                         |
| __gatt__eatt__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `RFCOMM <group_                  | The RFCOMM protocol provides     |
| _rfcomm__api__functions.html>`__ | emulation of serial ports over   |
|                                  | the L2CAP protocol               |
+----------------------------------+----------------------------------+
| `Service Discovery Protocol      | The Service Discovery Protocol   |
| (SDP) <group__sdp.html>`__       | (SDP) allows a device to         |
|                                  | discover services offered by     |
|                                  | other devices, and their         |
|                                  | associated parameters            |
+----------------------------------+----------------------------------+
| `Bluetooth Stack Platform        | Interface between Stack and      |
| Interface <group__wic            | platform                         |
| ed__bt__platform__group.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Memory                          | Helper APIs to create heaps and  |
| Manage                           | pools and allocate/free buffers  |
| ment <group__wiced__mem.html>`__ | from those pools or heaps        |
+----------------------------------+----------------------------------+
| `Common Bluetooth                | Common Bluetooth definitions     |
| defin                            |                                  |
| itions <group__gentypes.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `Wiced BT                        | WICED BT Types                   |
| Types <group__bt__types.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `WICED                           | **Result types** See             |
| result <group___result.html>`__  | wiced_result.h                   |
+----------------------------------+----------------------------------+
| `Timer Management                | Defines the interfaces for Timer |
| Services <group__timer.html>`__  | Management Services              |
+----------------------------------+----------------------------------+


.. toctree::
   :hidden:
   
   group__l2cap.rst
   group__wicedbt__avdt.rst
   group__wicedbt__avrc.rst
   group__wiced__bt__cfg.rst
   group__wicedbt___device_management.rst
   group__wicedbt__gatt.rst
   group__rfcomm__api__functions.rst
   group__sdp.rst
   group__wiced__bt__platform__group.rst
   group__wiced__mem.rst
   group__gentypes.rst
   group__timer.rst
   
   


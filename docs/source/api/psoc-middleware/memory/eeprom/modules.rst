==============
API Reference
==============

.. raw:: html

   <hr>

.. toctree::
   :hidden:

   group__group__em__eeprom__macros.rst
   group__group__em__eeprom__functions.rst
   group__group__em__eeprom__data__structures.rst
   group__group__em__eeprom__enums.rst
   

Provides the list of API Reference

+----------------------------------+----------------------------------+
|  \ `Macros <group__gr            | This section describes the       |
| oup__em__eeprom__macros.html>`__ | Emulated EEPROM Macros           |
+----------------------------------+----------------------------------+
|  \ `Functions <group__group      | This section describes the       |
| __em__eeprom__functions.html>`__ | Emulated EEPROM Function         |
|                                  | Prototypes                       |
+----------------------------------+----------------------------------+
|  \ `Data                         | This section describes the data  |
| Structures <group__group__em__e  | structures defined by the        |
| eprom__data__structures.html>`__ | Emulated EEPROM                  |
+----------------------------------+----------------------------------+
|  \ `Enumerated                   | This section describes the       |
| Types <group__g                  | enumeration types defined by the |
| roup__em__eeprom__enums.html>`__ | Emulated EEPROM                  |
+----------------------------------+----------------------------------+


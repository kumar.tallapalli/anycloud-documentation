=================
Enumerated Types
=================

.. doxygengroup:: group_canfd_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
====================
I2S (Inter-IC Sound)
====================

.. doxygengroup:: group_i2s
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

.. toctree::

   group__group__i2s__macros.rst
   group__group__i2s__functions.rst
   group__group__i2s__data__structures.rst
   group__group__i2s__enums.rst
   



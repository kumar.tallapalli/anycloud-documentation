================
Enumerated Types
================

.. doxygengroup:: group_sysint_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
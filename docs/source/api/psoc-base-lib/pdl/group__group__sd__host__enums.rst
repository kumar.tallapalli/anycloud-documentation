=================
Enumerated Types
=================


.. doxygengroup:: group_sd_host_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sysanalog" kind="group">
    <compoundname>group_sysanalog</compoundname>
    <title>SysAnalog    (System Analog Reference Block)</title>
    <innergroup refid="group__group__sysanalog__macros">Macros</innergroup>
    <innergroup refid="group__group__sysanalog__functions">Functions</innergroup>
    <innergroup refid="group__group__sysanalog__globals">Global Variables</innergroup>
    <innergroup refid="group__group__sysanalog__data__structures">Data Structures</innergroup>
    <innergroup refid="group__group__sysanalog__enums">Enumerated Types</innergroup>
    <briefdescription>
<para>This driver provides an interface for configuring the Analog Reference (AREF) block and querying the INTR_CAUSE register of the PASS. </para>    </briefdescription>
    <detaileddescription>
<para>The functions and other declarations used in this driver are in cy_sysanalog.h. You can include cy_pdl.h to get access to all functions and declarations in the PDL.</para><para>The AREF block has the following features:</para><para><itemizedlist>
<listitem><para>Generates a voltage reference (VREF) from one of three sources:<itemizedlist>
<listitem><para>Local 1.2 V reference (<bold>low noise, optimized for analog performance</bold>)</para></listitem><listitem><para>Reference from the SRSS (high noise, not recommended for analog performance)</para></listitem><listitem><para>An external pin</para></listitem></itemizedlist>
</para></listitem><listitem><para>Generates a 1 uA "zero dependency to absolute temperature" (IZTAT) current reference that is independent of temperature variations. It can come from one of two sources:<itemizedlist>
<listitem><para>Local reference (<bold>low noise, optimized for analog performance</bold>)</para></listitem><listitem><para>Reference from the SRSS (high noise, not recommended for analog performance)</para></listitem></itemizedlist>
</para></listitem><listitem><para>Generates a "proportional to absolute temperature" (IPTAT) current reference</para></listitem><listitem><para>Option to enable local references in Deep Sleep mode</para></listitem></itemizedlist>
</para><para>The locally generated references are the recommended sources for blocks in the PASS because they have tighter accuracy, temperature stability, and lower noise than the SRSS references. The SRSS references can be used to save power if the low accuracy and higher noise can be tolerated.</para><para><image name="aref_block_diagram.png" type="html" />
 
</para><para>The outputs of the AREF are consumed by multiple blocks in the PASS and by the CapSense (CSDv2) block. In some cases, these blocks have the option of using the references from the AREF. This selection would be in the respective drivers for these blocks. In some cases, these blocks require the references from the AREF to function.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;AREF Output&lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;&lt;a href="group__group__sar.html#"&gt;SAR&lt;/a&gt;&lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;&lt;a href="group__group__ctdac.html#"&gt;CTDAC&lt;/a&gt;&lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;&lt;a href="group__group__ctb.html#"&gt;CTB&lt;/a&gt;&lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;CSDv2 &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;VREF &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;optional &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;optional &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;&lt;ndash /&gt; &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;optional  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;IZTAT &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;&lt;b&gt;required&lt;/b&gt; &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;&lt;ndash /&gt; &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;optional &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;optional  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;IPTAT &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;&lt;ndash /&gt; &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;&lt;ndash /&gt; &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;&lt;b&gt;required&lt;/b&gt; &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;&lt;ndash /&gt;  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para><para>This driver provides a function to query the INTR_CAUSE register of the PASS. There are two interrupts in the PASS:</para><para><orderedlist>
<listitem><para>one global interrupt for all CTBs (up to 4)</para></listitem><listitem><para>one global interrupt for all CTDACs (up to 4)</para></listitem></orderedlist>
</para><para>Because the interrupts are global, the INTR_CAUSE register is needed to query which hardware instance triggered the interrupt.</para>
<para><heading level="1">Usage</heading></para>

<para><bold>Initialization</bold></para>
<para>To configure the AREF, call <ref kindref="member" refid="group__group__sysanalog__functions_1gacef4e5ae550518abd0004e46e5ec4d64">Cy_SysAnalog_Init</ref> and provide a pointer to the configuration structure, <ref kindref="compound" refid="structcy__stc__sysanalog__config__t">cy_stc_sysanalog_config_t</ref>. Three predefined structures are provided in this driver to cover a majority of use cases:</para><para><itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sysanalog__globals_1ga85e9e1cd8c1e6fe581043cbab3083017">Cy_SysAnalog_Fast_Local</ref> <bold>(recommended for analog performance)</bold></para></listitem><listitem><para><ref kindref="member" refid="group__group__sysanalog__globals_1gaf0a91cc831b1e5b70b8c4327a571d4b4">Cy_SysAnalog_Fast_SRSS</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__sysanalog__globals_1ga2bed42fba0641096ff28cdb776eb1548">Cy_SysAnalog_Fast_External</ref></para></listitem></itemizedlist>
</para><para>After initialization, call <ref kindref="member" refid="group__group__sysanalog__functions_1ga153249c7b4cea72ee334ae877a211169">Cy_SysAnalog_Enable</ref> to enable the hardware.</para>

<para><bold>Deep Sleep Operation</bold></para>
<para>The AREF current and voltage references can be enabled to operate in Deep Sleep mode with <ref kindref="member" refid="group__group__sysanalog__functions_1ga5d6d537beaf82fddf8e06f30b1d308b3">Cy_SysAnalog_SetDeepSleepMode</ref>. There are four options for Deep Sleep operation:</para><para><itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sysanalog__enums_1gga0aadb061d882d9a964dff3729322cf12a00f19ce656942fbb30c5cff00c891dc6">CY_SYSANALOG_DEEPSLEEP_DISABLE</ref> : Disable AREF IP block</para></listitem><listitem><para><ref kindref="member" refid="group__group__sysanalog__enums_1gga0aadb061d882d9a964dff3729322cf12a040af8630904ae7b6e969a18c2712a70">CY_SYSANALOG_DEEPSLEEP_IPTAT_1</ref> : Enable IPTAT generator for fast wakeup from Deep Sleep mode. IPTAT outputs for CTBs are disabled.</para></listitem><listitem><para><ref kindref="member" refid="group__group__sysanalog__enums_1gga0aadb061d882d9a964dff3729322cf12ac3b1263aa8889f21a72a8ec9d21066b2">CY_SYSANALOG_DEEPSLEEP_IPTAT_2</ref> : Enable IPTAT generator and IPTAT outputs for CTB</para></listitem><listitem><para><ref kindref="member" refid="group__group__sysanalog__enums_1gga0aadb061d882d9a964dff3729322cf12aaf8feaa23b7bbbdaba3981fa7e9a64de">CY_SYSANALOG_DEEPSLEEP_IPTAT_IZTAT_VREF</ref> : Enable all generators and outputs: IPTAT, IZTAT, and VREF</para></listitem></itemizedlist>
</para><para>Recall that the CTB requires the IPTAT reference. For the CTB to operate at the 1 uA current mode in Deep Sleep mode, the AREF must be enabled for <ref kindref="member" refid="group__group__sysanalog__enums_1gga0aadb061d882d9a964dff3729322cf12aaf8feaa23b7bbbdaba3981fa7e9a64de">CY_SYSANALOG_DEEPSLEEP_IPTAT_IZTAT_VREF</ref>. For the CTB to operate at the 100 nA current mode in Deep Sleep mode, the AREF must be enabled for <ref kindref="member" refid="group__group__sysanalog__enums_1gga0aadb061d882d9a964dff3729322cf12ac3b1263aa8889f21a72a8ec9d21066b2">CY_SYSANALOG_DEEPSLEEP_IPTAT_2</ref> minimum. In this lower current mode, the AREF IPTAT must be redirected to the CTB IZTAT. See the high level function <ref kindref="member" refid="group__group__ctb__functions__aref_1ga5a5dfa4b6150d4ba15f05a5c5ccdcaae">Cy_CTB_SetCurrentMode</ref> in the CTB PDL driver.</para><para>If the CTDAC is configured to use the VREF in Deep Sleep mode, the AREF must be enabled for <ref kindref="member" refid="group__group__sysanalog__enums_1gga0aadb061d882d9a964dff3729322cf12aaf8feaa23b7bbbdaba3981fa7e9a64de">CY_SYSANALOG_DEEPSLEEP_IPTAT_IZTAT_VREF</ref>.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The SRSS references are not available to the AREF in Deep Sleep mode. When operating in Deep Sleep mode, the local or external references must be selected.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>


<para><heading level="1">More Information</heading></para>
<para>For more information on the AREF, refer to the technical reference manual (TRM).</para>

<para><heading level="1">MISRA-C Compliance]</heading></para>
<para>This driver does not have any specific deviations.</para>

<para><heading level="1">Changelog</heading></para>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Version&lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Changes&lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Reason for Change &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;1.10.1 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Minor documentation updates. &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Documentation enhancement.  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td rowspan="2"&gt;&lt;p&gt;1.10 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Flattened the organization of the driver source code into the single source directory and the single include directory.  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Driver library directory-structure simplification.  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;Added register access layer. Use register access macros instead of direct register access using dereferenced pointers. &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Makes register access device-independent, so that the PDL does not need to be recompiled for each supported part number.  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;1.0 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Initial version &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p /&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>
    </detaileddescription>
  </compounddef>
</doxygen>
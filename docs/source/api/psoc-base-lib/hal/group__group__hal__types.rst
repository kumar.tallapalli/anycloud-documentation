============================
HAL General Types/Macros
============================

.. doxygengroup:: group_hal_types
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__result.rst
   group__group__hal__general__types.rst
   group__group__hal__override.rst
   group__group__hal__types__implementation.rst


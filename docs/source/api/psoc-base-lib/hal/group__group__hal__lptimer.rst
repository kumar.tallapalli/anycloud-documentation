==========================
LPTimer (Low-Power Timer)
==========================

.. doxygengroup:: group_hal_lptimer
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__lptimer.rst
   


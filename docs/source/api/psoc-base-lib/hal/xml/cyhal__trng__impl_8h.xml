<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__trng__impl_8h" kind="file" language="C++">
    <compoundname>cyhal_trng_impl.h</compoundname>
    <includes local="yes" refid="cyhal__trng_8h">cyhal_trng.h</includes>
    <includedby local="yes" refid="cyhal__trng_8c">cyhal_trng.c</includedby>
    <briefdescription>
<para>Provides an implementation of the Cypress TRNG HAL API. </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="copyright"><para>Copyright 2018-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="25"><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="26"><highlight class="normal" /></codeline>
<codeline lineno="27"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_trng.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_IP_MXCRYPTO)</highlight><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="33"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />__cplusplus<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal" /><highlight class="comment">/*<sp />Initialization<sp />polynomial<sp />values<sp />for<sp />True<sp />Random<sp />Number<sp />Generator<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="36"><highlight class="normal" /><highlight class="preprocessor">#define<sp />CYHAL_GARO31_INITSTATE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0x04c11db7UL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /><highlight class="preprocessor">#define<sp />CYHAL_FIRO31_INITSTATE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0x04c11db7UL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /></codeline>
<codeline lineno="39"><highlight class="normal" /><highlight class="preprocessor">#define<sp />MAX_TRNG_BIT_SIZE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(32UL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="40"><highlight class="normal" /></codeline>
<codeline lineno="41"><highlight class="normal" /><highlight class="comment">//<sp />This<sp />helper<sp />function<sp />mirrors<sp />the<sp />definition<sp />of<sp />cyhal_trng_generate</highlight><highlight class="normal" /></codeline>
<codeline lineno="42"><highlight class="normal" /><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">inline</highlight><highlight class="normal"><sp />uint32_t<sp />_cyhal_trng_generate_internal(</highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__trng__t">cyhal_trng_t</ref><sp />*obj)</highlight></codeline>
<codeline lineno="43"><highlight class="normal">{</highlight></codeline>
<codeline lineno="44"><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(NULL<sp />!=<sp />obj);</highlight></codeline>
<codeline lineno="45"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />value;</highlight></codeline>
<codeline lineno="46"><highlight class="normal"><sp /><sp /><sp /><sp />cy_en_crypto_status_t<sp />status<sp />=<sp />Cy_Crypto_Core_Trng(</highlight></codeline>
<codeline lineno="47"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />obj-&gt;base,<sp />CYHAL_GARO31_INITSTATE,<sp />CYHAL_FIRO31_INITSTATE,<sp />MAX_TRNG_BIT_SIZE,<sp />&amp;value);</highlight></codeline>
<codeline lineno="48"><highlight class="normal"><sp /><sp /><sp /><sp />(void)status;</highlight></codeline>
<codeline lineno="49"><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(CY_CRYPTO_SUCCESS<sp />==<sp />status);</highlight></codeline>
<codeline lineno="50"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />value;</highlight></codeline>
<codeline lineno="51"><highlight class="normal">}</highlight></codeline>
<codeline lineno="52"><highlight class="normal" /></codeline>
<codeline lineno="53"><highlight class="normal" /><highlight class="preprocessor">#define<sp />cyhal_trng_generate(obj)<sp />_cyhal_trng_generate_internal(obj)</highlight><highlight class="normal" /></codeline>
<codeline lineno="54"><highlight class="normal" /></codeline>
<codeline lineno="55"><highlight class="normal" /></codeline>
<codeline lineno="56"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="57"><highlight class="normal">}</highlight></codeline>
<codeline lineno="58"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />__cplusplus<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="59"><highlight class="normal" /></codeline>
<codeline lineno="60"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />defined(CY_IP_MXCRYPTO)<sp />*/</highlight><highlight class="preprocessor" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_trng_impl.h" />
  </compounddef>
</doxygen>
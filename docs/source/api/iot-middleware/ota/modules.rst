====================
API Reference
====================


The following provides a list of driver API documentation

+-----------------------------------+-----------------------------------+
| `Cypress Over The Air (OTA)       | OTA support for downloading and   |
| API <group__group__cy__ota        | installing firmware updates       |
| .html>`__                         |                                   |
+-----------------------------------+-----------------------------------+
|  \ `OTA                           | Customer overrides for the OTA    |
| Configurations <group__group__ota | library                           |
| __config.html>`__                 |                                   |
+-----------------------------------+-----------------------------------+
|  \ `OTA                           | Macros used to define OTA Agent   |
| Macros <group__group__ota__macros | behavior                          |
| .html>`__                         |                                   |
+-----------------------------------+-----------------------------------+
|  \ `OTA                           | Typedefs used by the OTA Agent    |
| Typedefs <group__group__ota__type |                                   |
| defs.html>`__                     |                                   |
+-----------------------------------+-----------------------------------+
|  \ `OTA Agent                     | An application can get callbacks  |
| Callback <group__group__ota__call | from the OTA Agent                |
| back.html>`__                     |                                   |
+-----------------------------------+-----------------------------------+
|  \ `OTA                           | Structures used for passing       |
| Structures <group__group__ota__st | information to and from OTA Agent |
| ructures.html>`__                 |                                   |
+-----------------------------------+-----------------------------------+
|  \ `OTA                           | Functions to start/stop and query |
| Functions <group__group__ota__fun | the OTA Agent                     |
| ctions.html>`__                   |                                   |
+-----------------------------------+-----------------------------------+

.. toctree::
   :hidden:

   group__group__cy__ota.rst




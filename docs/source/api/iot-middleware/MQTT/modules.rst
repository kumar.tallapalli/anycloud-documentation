====================
API Reference
====================

         The following provides a list of API documentation

      

         +----------------------------------+----------------------------------+
         |  \ `API Call                     | This section provides the        |
         | Sequence <group__mq              | details of the API call sequence |
         | tt__api__call__sequence.html>`__ | for performing various MQTT      |
         |                                  | operations                       |
         +----------------------------------+----------------------------------+
         |  \ `AWS IoT Device               | This SDK provides MQTT 3.1.1     |
         | SDK <group__mqtt__aws__iot       | client library implementation    |
         | __device__sdk__function.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  \ `Platform Port                | Cypress platform port layer      |
         | Lay\                             | functions and configurations     |
         | er <group__mqtt__cyport.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  \ `Functions <group_            | Cypress platform port layer      |
         | _mqtt__cyport__function.html>`__ | functions                        |
         +----------------------------------+----------------------------------+
         |  \ `Configurations <grou         | Cypress platform port layer      |
         | p__mqtt__cyport__config.html>`__ | configurations                   |
         +----------------------------------+----------------------------------+

.. toctree::
   :hidden:

   group__mqtt__api__call__sequence.rst
   group__mqtt__aws__iot__device__sdk__function.rst
   group__mqtt__cyport.rst
   
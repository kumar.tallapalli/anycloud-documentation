======
C API
======

.. doxygengroup:: group_c_api
   :project: http-server

.. toctree::
   
   group__http__server__struct.rst
   group__http__server__defines.rst
   group__group__c__api__functions.rst